/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 */
Ext.define('DemoApp.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    requires: [
        'Ext.MessageBox',
        'Ext.layout.Fit'
    ],
    controller: 'main',
    viewModel: 'main',
    defaults: {
        tab: {
            iconAlign: 'top'
        }
    },
    tabBarPosition: 'bottom',
    items: [{
        xtype: 'panel',
        title: 'Users',
        iconCls: 'x-fa fa-user',
        ui: 'red-panel',
        layout: 'vbox',
        html: 'Main Panel with RED UI',
        defaults: {
            xtype: 'panel',
            flex: 1,
            margin: 5,
            padding: 20
        },
        items: [{
            ui: 'green-panel',
            html: 'Panel with green UI'
        }, {
            ui: 'transparent-panel',
            html: 'Panel with transparent UI'
        }]
    }, {
        title: 'Home',
        iconCls: 'x-fa fa-home',
        layout: 'fit',
        // The following grid shares a store with the classic version's grid as well!
        items: [{
            xtype: 'mainlist'
        }]
    }, {
        title: 'Groups',
        iconCls: 'x-fa fa-users',
        bind: {
            html: '{loremIpsum}'
        }
    }, {
        title: 'Settings',
        iconCls: 'x-fa fa-cog',
        bind: {
            html: '{loremIpsum}'
        }
    }]
});